
These are the KiCad files for a new implementation of David Conroy's PDP-10/X System Board
(http://www.fpgaretrocomputing.org/pdp10x).

Full details are available at https://www.retrobrewcomputers.org/doku.php?id=builderpages:robg:pdp10x

Rob Gowin
01/27/2020
